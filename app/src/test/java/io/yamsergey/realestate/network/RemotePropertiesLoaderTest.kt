package io.yamsergey.realestate.network

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.yamsergey.realestate.emptyProperty
import io.yamsergey.realestate.network.dto.SearchResponse
import io.yamsergey.realestate.network.endpoint.PropertiesEndpoint
import kotlinx.coroutines.runBlocking
import org.junit.Test
import retrofit2.Response

internal class RemotePropertiesLoaderTest {

    private val propertiesEndpoint = mock<PropertiesEndpoint>()
    private val networkMonitor = mock<NetworkMonitor>()

    private val instance = RemotePropertiesLoader(propertiesEndpoint, networkMonitor)


    @Test
    fun `should return properties list`() {
        runBlocking {
            given(propertiesEndpoint.search(any(), any())).willReturn(
                Response.success(
                    responseTemplate.copy(
                        items = listOf(
                            emptyProperty
                        )
                    )
                )
            )

            assert(instance.search("").contains(emptyProperty))
        }
    }

    @Test
    fun `should request next page`() {
        runBlocking {
            given(propertiesEndpoint.search(any(), any())).willReturn(
                Response.success(
                    responseTemplate.copy(
                        items = listOf(
                            emptyProperty
                        ),
                        hasNextPage = true
                    )
                )
            )

            instance.search("")
            instance.search("")

            verify(propertiesEndpoint).search("", 1)
        }
    }

    @Test
    fun `should request new page for new query`() {
        runBlocking {
            given(propertiesEndpoint.search(any(), any())).willReturn(
                Response.success(
                    responseTemplate.copy(
                        items = listOf(
                            emptyProperty
                        ),
                        hasNextPage = true
                    )
                )
            )

            instance.search("")
            instance.search("new")

            verify(propertiesEndpoint).search("new", 0)
        }
    }

    private val responseTemplate = SearchResponse(
        resultCount = 1,
        start = 0,
        page = 0,
        pageCount = 1,
        itemsPerPage = 10,
        hasNextPage = false,
        hasPreviousPage = false,
        items = listOf(emptyProperty)
    )

}