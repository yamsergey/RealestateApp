package io.yamsergey.realestate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.Snackbar
import io.yamsergey.realestate.ktx.viewModel
import io.yamsergey.realestate.ui.PropertiesListViewModel
import io.yamsergey.realestate.ui.PropertiesAdapter

typealias Layouts = R.layout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(Layouts.list_screen)

        val viewModel: PropertiesListViewModel by viewModel {
            PropertiesListViewModel(
                (application as App).repository,
                (application as App).propertiesServiceFactory.createPropertiesService()
            )
        }

        val realEstateAdapter = PropertiesAdapter(viewModel)

        findViewById<RecyclerView>(R.id.list).adapter = realEstateAdapter

        viewModel.realEstatesList().observe(
            this,
            Observer {
                it?.run {
                    realEstateAdapter.submitList(it)
                }
            })

        viewModel.networkMonitor.errors.observe(this, Observer {
            Snackbar.make(
                findViewById<View>(android.R.id.content).rootView,
                R.string.network_error,
                LENGTH_LONG
            )
        })

        viewModel.executeSearch()
    }
}