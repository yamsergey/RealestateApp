package io.yamsergey.realestate.network

import io.yamsergey.realestate.network.endpoint.PropertiesEndpoint
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


internal class StubRetrofitServiceFactory(
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build(),
    private val networkMonitor: NetworkMonitor = NetworkMonitor()
) : ServiceFactory {

    override fun createPropertiesService(): PropertiesService =
        RemotePropertiesLoader(retrofit.create(PropertiesEndpoint::class.java), networkMonitor)

    companion object {
        const val URL = "https://private-492e5-homegate1.apiary-mock.com/"
    }
}

