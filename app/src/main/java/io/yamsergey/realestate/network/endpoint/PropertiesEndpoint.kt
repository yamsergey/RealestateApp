package io.yamsergey.realestate.network.endpoint

import io.yamsergey.realestate.network.dto.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PropertiesEndpoint {

    @GET("properties")
    suspend fun search(@Query("query") query: String, @Query("page") page: Int): Response<SearchResponse>
}