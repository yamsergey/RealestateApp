package io.yamsergey.realestate.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

internal open class NetworkMonitor {

    private val networkError = MutableLiveData<NetworkError>()

    val errors: LiveData<NetworkError> = networkError

    fun postError(error: NetworkError) = networkError.postValue(error)
}