package io.yamsergey.realestate.network

internal interface ServiceFactory {

    fun createPropertiesService(): PropertiesService
}