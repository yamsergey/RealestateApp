package io.yamsergey.realestate.network

import io.yamsergey.realestate.network.dto.Property
import io.yamsergey.realestate.network.dto.SearchResponse
import io.yamsergey.realestate.network.endpoint.PropertiesEndpoint

internal class RemotePropertiesLoader(
    private val endpoint: PropertiesEndpoint,
    override val networkMonitor: NetworkMonitor
) :
    PropertiesService {

    private var lastResponse: SearchResponse? = null
    private var lastQuery: String? = null

    override suspend fun search(query: String): List<Property> {
        invalidateQuery(query)

        return if (lastResponse?.hasNextPage != false) {

            val response = endpoint.search(query, lastResponse.nextPage())

            if (!response.isSuccessful) {
                networkMonitor.postError(RequestFailed(response.code()))
                emptyList()
            } else {
                val body = response.body()

                /**
                 * Work around to make pagination works.
                 * Server always respond with the same data, hence we should
                 * break infinity loop.
                 * */
                if (lastResponse != body) {
                    lastResponse = body
                    if (response.isSuccessful && body != null) {
                        body.items
                    } else {
                        emptyList()
                    }
                } else {
                    lastResponse = lastResponse?.copy(hasNextPage = false)
                    emptyList()
                }
            }
        } else {
            emptyList()
        }
    }

    private fun invalidateQuery(query: String) {
        if (query != lastQuery) {
            lastResponse = null
            lastQuery = query
        }
    }

    private fun SearchResponse?.nextPage() = if (this != null) this.page + 1 else 0

}