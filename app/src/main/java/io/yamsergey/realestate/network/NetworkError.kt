package io.yamsergey.realestate.network

sealed class NetworkError

data class RequestFailed(val errorCode: Int) : NetworkError()
object NoNetwork : NetworkError()