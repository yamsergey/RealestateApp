package io.yamsergey.realestate.network.dto

import com.google.gson.annotations.SerializedName

data class ExternalUrl(
    @SerializedName("url") val url: String,
    @SerializedName("type") val type: String,
    @SerializedName("label") val label: String
)