package io.yamsergey.realestate.network

import io.yamsergey.realestate.network.dto.Property

internal interface PropertiesService {

    suspend fun search(query: String): List<Property>

    val networkMonitor: NetworkMonitor
}