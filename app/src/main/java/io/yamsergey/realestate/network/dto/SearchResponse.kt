package io.yamsergey.realestate.network.dto

data class SearchResponse(
    val resultCount: Int,
    val start: Int,
    val page: Int,
    val pageCount: Int,
    val itemsPerPage: Int,
    val hasNextPage: Boolean,
    val hasPreviousPage: Boolean,
    val items: List<Property>
)