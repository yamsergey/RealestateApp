package io.yamsergey.realestate.ui

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.yamsergey.realestate.model.Property
import io.yamsergey.realestate.model.Repository
import io.yamsergey.realestate.network.NetworkMonitor
import io.yamsergey.realestate.network.PropertiesService
import kotlinx.coroutines.launch
import java.util.*

typealias PropertyDto = io.yamsergey.realestate.network.dto.Property

internal class PropertiesListViewModel(
    private val repository: Repository,
    private val propertiesService: PropertiesService
) : ViewModel() {

    private lateinit var currentQuery: String


    private val boundaryCallback = object : PagedList.BoundaryCallback<Property>() {
        override fun onItemAtEndLoaded(itemAtEnd: Property) {
            super.onItemAtEndLoaded(itemAtEnd)
            launchSearch()
        }
    }

    private fun launchSearch() {
        viewModelScope.launch {
            val properties = propertiesService.search(currentQuery).map { it.toDomain() }
            val existed =
                repository.properties(withIds = properties.map { it.id }).map { it.id to it }
                    .toMap()

            properties.forEach {
                it.favorite = existed[it.id]?.favorite ?: it.favorite
            }
            repository.persist(properties)
        }
    }

    val networkMonitor: NetworkMonitor = propertiesService.networkMonitor

    fun realEstatesList(): LiveData<PagedList<Property>> =
        LivePagedListBuilder(repository.properties(),
            PAGE_SIZE
        ).setBoundaryCallback(
            boundaryCallback
        ).build()

    fun executeSearch(query: String = QUERY) {
        currentQuery = query
        launchSearch()
    }

    fun toggleFavorite(property: Property) = viewModelScope.launch {
        repository.persist(property.copy(favorite = !property.favorite))
    }

    companion object {
        const val QUERY = "Zurich"
        const val PAGE_SIZE = 10
    }
}


@SuppressLint("DefaultLocale")
internal fun PropertyDto.toDomain() =
    Property(
        id = advertisementId,
        images = listOf(hardcodedImages.random()),
        price = price,
        favorite = false,
        location = street,
        label = title.capitalize(),
        timestamp = Date().time,
        currency = currency,
        score = score,
        realtorImage = realtorsImages.random()
    )


// Just to have so placeholder images
private val hardcodedImages = listOf(
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1590433478/listings/hgoi/images/202005252048497559492.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1585161230/listings/hgof/images/202003251932594935410.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1588676906/listings/x472/images/ff0000000002483937.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1588676904/listings/x472/images/ff0000000002483914.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1562066078/listings/x472/images/ff0000000001347881.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1584968890/listings/x472/images/ff0000000002333871.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1584961581/listings/x472/images/ff0000000002333852.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1562066080/listings/x472/images/ff0000000001348171.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1565787724/listings/c367/images/201604221036147486648.jpg",
    "https://media.homegate.ch/image/upload/f_auto/t_web_dp_large/v1562066062/listings/x472/images/ff0000000001340751.jpg"
)

// Fancy realtors
private val realtorsImages = listOf(
    "https://www.pikpng.com/pngl/m/6-63707_download-bart-simpson-head-png-clipart.png",
    "https://www.vhv.rs/dpng/d/419-4198949_stormtrooper-mask-png-image-storm-trooper-helmet-transparent.png",
    "https://i7.pngguru.com/preview/559/169/225/albert-einstein-scientist-matt-pattinson-space-insanity-doing-the-same-thing-over-and-over-again-and-expecting-different-results-albert-einstein.jpg",
    "https://i7.pngflow.com/pngimage/361/589/png-tmnt-leonardo-head-illustration-leonardo-michaelangelo-donatello-teenage-mutant-ninja-turtles-teenage-mutant-ninja-turtles-masks-face-smiley-painting-emoticon-clipart.png"
)