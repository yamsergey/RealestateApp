package io.yamsergey.realestate.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import io.yamsergey.realestate.R
import io.yamsergey.realestate.model.Property

internal object PropertiesDiffCallback : DiffUtil.ItemCallback<Property>() {
    override fun areItemsTheSame(oldItem: Property, newItem: Property): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Property, newItem: Property): Boolean {
        return oldItem == newItem
    }
}

internal class PropertiesAdapter(private val model: PropertiesListViewModel) :
    PagedListAdapter<Property, PropertyViewHolder>(PropertiesDiffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PropertyViewHolder {
        return PropertyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.property_list_item, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: PropertyViewHolder, position: Int) {
        val property = getItem(position) as Property
        holder.realtorLabel.text = property.location
        holder.realtorName.text = property.label
        holder.realtorScore.text = "${property.score}"
        holder.propertyPrice.text = "${property.price} ${property.currency}"
        holder.propertyImage.setImageURI(property.images.first())
        holder.realtorIcon.setImageURI(property.realtorImage)
        holder.propertyFavorite.setImageResource(if (property.favorite) R.drawable.ic_favorite else R.drawable.ic_favorite_border)
        holder.propertyFavorite.contentDescription =
            if (property.favorite) holder.itemView.context.getString(R.string.remove_favorite) else holder.itemView.context.getString(
                R.string.make_favorite
            )
        holder.propertyFavorite.setOnClickListener {
            model.toggleFavorite(property)
        }
    }
}