package io.yamsergey.realestate.ui

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.yamsergey.realestate.R

class PropertyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val realtorIcon = view.findViewById<SimpleDraweeView>(R.id.realtorIcon)
    val realtorName = view.findViewById<TextView>(R.id.realtorName)
    val realtorLabel = view.findViewById<TextView>(R.id.realtorLabel)
    val realtorScore = view.findViewById<TextView>(R.id.realtorScore)
    val propertyImage = view.findViewById<SimpleDraweeView>(R.id.realtorPropertyImage)
    val propertyPrice = view.findViewById<TextView>(R.id.realtorPropertyPrice)
    val propertyFavorite = view.findViewById<FloatingActionButton>(R.id.propertyFavorite)
}