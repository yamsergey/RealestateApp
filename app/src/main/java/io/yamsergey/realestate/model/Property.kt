package io.yamsergey.realestate.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "real_estate_items")
data class Property(
    @PrimaryKey
    var id: Int,
    var images: List<String>,
    var price: Int,
    var location: String,
    var label: String,
    var timestamp: Long,
    var favorite: Boolean,
    var score: Int,
    var currency: String,
    var realtorImage: String
)