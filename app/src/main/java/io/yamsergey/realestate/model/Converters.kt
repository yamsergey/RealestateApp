package io.yamsergey.realestate.model

import androidx.room.TypeConverter

internal class Converters {

    companion object {

        private const val separator = ";"

        @TypeConverter
        @JvmStatic
        fun fromStringList(list: List<String>) = list.joinToString(separator)

        @TypeConverter
        @JvmStatic
        fun toStringList(input: String) = input.split(separator)
    }
}