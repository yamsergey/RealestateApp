package io.yamsergey.realestate.model

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query

@Dao
interface PropertiesDao : AppDao<Property> {

    @Query("SELECT * FROM real_estate_items WHERE id = :id")
    suspend fun byId(id: Int): Property

    @Query("SELECT * FROM real_estate_items WHERE id IN (:ids)")
    suspend fun byIds(ids: List<Int>): List<Property>

    @Query("SELECT * FROM real_estate_items WHERE real_estate_items.timestamp > :after")
    fun all(after: Long = 0): DataSource.Factory<Int, Property>
}
