package io.yamsergey.realestate.model

class Repository(private val propertiesDao: PropertiesDao) {

    fun properties(after: Long = 0) = propertiesDao.all(after = after)

    suspend fun properties(withIds: List<Int>) = propertiesDao.byIds(ids = withIds)

    suspend fun persist(properties: List<Property>) = propertiesDao.add(properties)

    suspend fun persist(property: Property) = propertiesDao.add(property)
}