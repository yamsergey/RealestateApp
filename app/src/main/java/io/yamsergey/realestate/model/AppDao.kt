package io.yamsergey.realestate.model

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy

interface AppDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(value: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(value: List<T>)

    @Delete
    suspend fun remove(value: T)

    @Delete
    suspend fun remove(value: List<T>)

}