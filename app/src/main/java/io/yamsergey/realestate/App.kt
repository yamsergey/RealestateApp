package io.yamsergey.realestate

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import io.yamsergey.realestate.model.RealEstateDatabase
import io.yamsergey.realestate.model.Repository
import io.yamsergey.realestate.network.NetworkMonitor
import io.yamsergey.realestate.network.ServiceFactory
import io.yamsergey.realestate.network.StubRetrofitServiceFactory

internal class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }

    private fun database(): RealEstateDatabase {
        return RealEstateDatabase.getInstance(this)
    }

    val repository by lazy { Repository(database().realEstatesDao()) }

    var propertiesServiceFactory: ServiceFactory = StubRetrofitServiceFactory()

}
