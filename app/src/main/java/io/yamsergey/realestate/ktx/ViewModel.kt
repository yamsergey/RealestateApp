package io.yamsergey.realestate.ktx

import androidx.activity.ComponentActivity
import androidx.annotation.MainThread
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class SingleViewModelFactory<V : ViewModel>(private val creator: () -> V) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return creator() as T
    }
}

@MainThread
inline fun <reified VM : ViewModel> ComponentActivity.viewModel(
    noinline instanceProducer: (() -> ViewModel)
): Lazy<VM> {
    val factoryPromise = { SingleViewModelFactory(instanceProducer) }

    return ViewModelLazy(VM::class, { viewModelStore }, factoryPromise)
}