package io.yamsergey.realestate

import io.yamsergey.realestate.network.NetworkMonitor
import io.yamsergey.realestate.network.PropertiesService
import io.yamsergey.realestate.network.RemotePropertiesLoader
import io.yamsergey.realestate.network.ServiceFactory
import io.yamsergey.realestate.network.endpoint.PropertiesEndpoint
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


internal class MockRetrofitServiceFactory(
    url: HttpUrl,
    private val networkMonitor: NetworkMonitor
) : ServiceFactory {

    private val retrofit: Retrofit

    init {
        val okHttpClient = OkHttpClient().newBuilder()
            .connectTimeout(40, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    override fun createPropertiesService(): PropertiesService =
        RemotePropertiesLoader(
            endpoint = retrofit.create(PropertiesEndpoint::class.java),
            networkMonitor = networkMonitor
        )
}