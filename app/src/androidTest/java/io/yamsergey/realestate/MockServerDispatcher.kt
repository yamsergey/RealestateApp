package io.yamsergey.realestate

import android.content.Context
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import java.lang.Exception

internal class MockServerDispatcher(private val context: Context) : Dispatcher() {

    override fun dispatch(request: RecordedRequest): MockResponse {
        val resources = context.resources
        return try {
            val assets = resources.assets
            val body = assets.open("fake_properties_response.json").bufferedReader()
                .use { it.readText() }
            MockResponse().setResponseCode(200).setBody(
                body
            )
        } catch (err: Exception) {
            MockResponse().setResponseCode(200).setBody("")
        }
    }
}