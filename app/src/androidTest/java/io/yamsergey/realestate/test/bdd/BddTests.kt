package io.yamsergey.realestate.test.bdd

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.swipeDown
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import cucumber.api.CucumberOptions
import cucumber.api.java.After
import cucumber.api.java.Before
import cucumber.api.java.en.Given
import io.yamsergey.realestate.App
import io.yamsergey.realestate.MainActivity
import io.yamsergey.realestate.MockRetrofitServiceFactory
import io.yamsergey.realestate.MockServerDispatcher
import io.yamsergey.realestate.network.NetworkMonitor
import io.yamsergey.realestate.test.bdd.utils.PropertiesFavoriteClick
import io.yamsergey.realestate.test.bdd.utils.RecyclerViewItemCountAssertion.Companion.withItemCount
import io.yamsergey.realestate.ui.PropertyViewHolder
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.core.IsEqual


typealias Ids = io.yamsergey.realestate.R.id
typealias Strings = io.yamsergey.realestate.R.string


@CucumberOptions(
    features = ["features"]
)
class BddTests {

    private var rule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java, false, false
    )

    private lateinit var webServer: MockWebServer

    @Before
    fun before() {
        val targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        val app = targetContext.applicationContext as App
        webServer = MockWebServer()
        webServer.dispatcher =
            MockServerDispatcher(InstrumentationRegistry.getInstrumentation().context)
        webServer.start()
        app.propertiesServiceFactory = MockRetrofitServiceFactory(webServer.url("/"), NetworkMonitor())
    }

    @After
    @Throws(Exception::class)
    fun finishActivity() {
        rule.activity.finish()
        webServer.shutdown()
    }

    @Given("Open properties list")
    fun launchHomeScreen() {
        rule.launchActivity(null)
    }

    @Given("Swipe properties list {string}")
    fun swipe(direction: String) {
        if ("up" == direction) {
            onView(withId(Ids.list)).perform(swipeUp())
        } else if ("down" == direction) {
            onView(withId(Ids.list)).perform(swipeDown())
        }
    }

    @Given("Properties list has {int} items")
    fun propertiesListHasNumberOfItems(number: Int) {
        onView(withId(Ids.list)).check(withItemCount(IsEqual(number.toInt())))
    }

    @Given("Scroll to property {int}")
    fun scrollToProperty(position: Int) {
        onView(withId(Ids.list)).perform(
            RecyclerViewActions.scrollToPosition<PropertyViewHolder>(position)
        )
    }

    @Given("Make property {int} favorite")
    fun makePropertyFavorite(position: Int) {
        scrollToProperty(position)
        onView(withId(Ids.list)).perform(RecyclerViewActions.actionOnItemAtPosition<PropertyViewHolder>(position, PropertiesFavoriteClick()))
    }

    @Given("Verify property {int} is favorite")
    fun verifyPropertyIsFavorite(position: Int) {
        onView(withId(Ids.list)).perform(
            RecyclerViewActions.scrollToPosition<PropertyViewHolder>(position)
        )

        onView(withContentDescription(Strings.remove_favorite)).check(matches(isDisplayed()))
    }

    @Given("Close properties list")
    fun closePropertiesList() {
        rule.finishActivity()
    }

}