package io.yamsergey.realestate.test.bdd.utils

import android.view.View
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.matcher.ViewMatchers
import org.hamcrest.MatcherAssert

typealias Strings = io.yamsergey.realestate.R.string

internal class FavoritePropertyAssertion(): ViewAssertion {

    override fun check(view: View?, noViewFoundException: NoMatchingViewException?) {

        noViewFoundException?.run { throw noViewFoundException }
        MatcherAssert.assertThat(view,
            ViewMatchers.hasDescendant(ViewMatchers.withContentDescription(Strings.remove_favorite))
        )
    }

    companion object {

        fun hasFavorite() = FavoritePropertyAssertion()
    }

}