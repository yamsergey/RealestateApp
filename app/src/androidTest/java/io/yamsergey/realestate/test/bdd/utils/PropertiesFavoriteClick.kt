package io.yamsergey.realestate.test.bdd.utils

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import io.yamsergey.realestate.R
import org.hamcrest.Matcher

internal class PropertiesFavoriteClick: ViewAction {
    override fun getDescription(): String = "Tap on favorite icon"

    override fun getConstraints(): Matcher<View>? = null

    override fun perform(uiController: UiController, view: View) {
        view.findViewById<View>(R.id.propertyFavorite).performClick()
    }
}