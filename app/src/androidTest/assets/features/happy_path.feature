Feature: Happy path for properties list

    Scenario: Open list and scroll through
        Given Open properties list
        Then Swipe properties list "up"
        Then Properties list has 10 items

    Scenario: Open list and make choose favorite
        Given Open properties list
        And Scroll to property 3
        And Make property 3 favorite
        And Close properties list
        And Open properties list
        Then Verify property 3 is favorite
