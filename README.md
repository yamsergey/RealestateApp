# Realesatte app

The repository contains example of real estate app, which show list of properties fetched from remote web server.



## Architecture

Architecture is MVVM (but without DataBinding library).

Inside the app `Dependency injection` is preferred way to provide dependencies (Just manual injections, no Dagger, and scope covered by class where the dependency has been created) 

*Model layer*
- Covers local storage and representation of network DTO to the app (Network `Property` and `Model` property are not the same).
- Also it provides paginated representation of data for `ViewModel``

*Network Layer*
- Fetch data from endpoint
- Covers pagination (when it exists) for network service 

*ViewModel*
- Provides data for `View` (Properties list and Network errors)
- React on user actions (Starts network requests for data and persist results)



```plantuml
@startuml

class App {
    Repository repository
    NetworkMonitor networkMonitor
    ServiceFactory serviceFactory
}

package "VIEW" {
    class MainActivity
    class PropertiesAdapter
    class PropertiesAdapter
}

package "VIEWMODEL" {
    class PropertiesListViewModel
}

package "MODEL" {
    class Repository
    class RealEstateDatabase
}

package "NETWORK" {
    class NetworkMonitor
    class PropertiesService
    class PropertiesEndpoint
}

 

App *-- Repository
App *-- RealEstateDatabase


PropertiesAdapter o-- PropertiesListViewModel
MainActivity o-- PropertiesListViewModel

PropertiesListViewModel o-- Repository
PropertiesListViewModel *-- PropertiesService

PropertiesService *-- PropertiesEndpoint
PropertiesService *-- NetworkMonitor

@enduml
```

## Testing

Integration and UI tests done by `Cucumber` + `Espresso` (Just run usual `./gralew connectedCheck`). Which allows to write test cases which are close to business requirements. Check `app/src/androidTest/assets/features/happy_path.feature` for example. Right now it's only happy path. But exception cases also can (and have to be covered).
Network requests replaced with mocked data to make the tests reproduceble.



Functionality which doesn't require Android instrumentation tested with `JUnit` + `Mockito`.



`Robolectric` tests was planned to cover database integration, but due to lack of time (and also lack of complex functionality) is not covered yet.



## CI/CD

Each commit tested by outomated pipeline (Thanks GitLab, because I currently I don't have free build time on GutHub and CircleCI).



## UI

![](realestate.gif) 